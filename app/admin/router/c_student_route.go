package router

import (
	"github.com/gin-gonic/gin"
	"go-admin/app/admin/apis"
)

func init() {
	routerNoCheckRole = append(routerNoCheckRole, registerCStudentRouter)
}

// registerSysApiRouter
func registerCStudentRouter(v1 *gin.RouterGroup) {
	api := apis.CStudentApi{}
	//r := v1.Group("/cStudent").Use(middleware.AuthCheckRole())
	r := v1.Group("/cStudent")
	{
		r.GET("", api.GetPage)
		//r.GET("/:id", api.Get)
		//r.PUT("/:id", api.Update)
		//r.POST("", api.Insert)
		//r.DELETE("", api.Delete)
	}
}
